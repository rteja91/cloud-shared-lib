def call(Map config){
    assert config.containsKey("compileType") : "ERROR 'compileType' is not defined in properties. Pipeline will exit"
    def compileVars = calculateCompileVarsMap(config)
    runCompileImage(compileVars)
}

def runCompileImage(Map compileVars){
    echo "INFO Attempting to login to registry '${compileVars['compileImageRegistry']}' using creds '${compileVars["compileRegistryCredsSet"]} to pull '${compileVars["compileImage"]}' "
    docker.withRegistry("https://${compileVars['compileImageRegistry']}", compileVars["compileRegistryCredsSet"]){
        echo "[INFO] Login successful"
        def credsEnvString = ""
        //withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialId: env.artifactoryCredentialsIdRO, usernameVariable: 'ARTIFACTORY_USERNAME', passwordVariable: 'ARTIFACTORY_PASSWORD' ]]){
        //    credsEnvString="-e 'ARTIFACTORY_USERNAME=${ARTIFACTORY_USERNAME}' -e 'ARTIFACTORY_PASSWORD=${ARTIFACTORY_PASSWORD}' "
        // sh script:  "docker run -i ${compileVars['compileMounts']} ${compileVars['customVarString']} ${credsEnvString} ${compileVars['compileImage']} ${compileVars['compileArgs']} "
             sh script:  "docker run -i ${compileVars['compileMounts']} ${compileVars['customVarString']}  ${compileVars['compileImage']} ${compileVars['compileArgs']} "

       // }

    }
}

def calculateCompileVarsMap(Map config){
    def compileType = config["compileType"]
    def customizableVars= (compileType == "custom" ) ? ['compileImage','compileArgs','compileRegistryUser', 'compileMounts', 'customVarString']:['compileImage','compileArgs','compileRegistryUser']
    pipelineLogger.debug("compileType=${compileType}")
    def compileVars=getCompileVarsMap(config["compileType"])
    for (var in customizableVars){
        if(config.containsKey(var)){
            compileVars.put(var,config[var])
        }
    }

    def customVarString = ""

    if(config["compileType"] != "custom")
    {
        pipelineLogger.debug("ready to generate custom compile var string")
        customVarString = generateCustomCompileVarString(config)
        pipelineLogger.debug("customVarString=${customVarString}")
    }else{
        customVarString = (config["customVarString"]!=null) ?  config["customVarString"] : ""
    }
        compileVars.put("customVarString", customVarString)
        compileVars = generateCompileVarsAuth(config, compileVars)
        pipelineLogger.debug("compileVars=${compileVars}")

    return compileVars
}

def getCompileVarsMap(String compileType){
    pipelineLogger("workspace folder ${env.WORKSPACE}", "DEBUG")
    def compileTypeToCompileVarsMap=[
        'maven':
        [
            'compileImage':'rteja91/base-images/maven-base:1.0.0',
            'compileArgs':'clean Package',
            'compileMounts':"-v ${env.WORKSPACE}:/mnt/app -v ${env.M2_PATH}:/mnt/.m2",
            'compileRegistryUser': ""
        ],
        'node':
        [
            'compileImage':'rteja91/node-builder:latest',
            'compileArgs':'clean Package',
            'compileMounts':"-v ${env.WORKSPACE}:/mnt -v ${env.NPMRC_PATH}:/home/ec2-user/.npmrc -v ${env.NPM_CACHE_PATH}:/home/ec2-user/.npm -e SHOULD_USE_NPM=true",
            'compileRegistryUser': ""
        ],
        'gradle':
        [
            'compileImage':'docker.io/gradle:4.7.0-jdk8-alpine',
            'compileArgs':'sh -c "gradle build --no-daemon"',
            'compileMounts':"-v ${env.WORKSPACE}:/home/gradle ",
            'compileRegistryUser':""
        ],
        'custom':
        [
            'compileImage':'',
            'compileArgs':'',
            'compileMounts':'',
            'compileRegistryUser':""
        ]
    ]
    assert compileTypeToCompileVarsMap.containsKey(compileType) : "ERROR: Invalid compileType defined in properties. Possible options are ${compileTypeToCompileVarsMap.keySet()}"
    return compileTypeToCompileVarsMap[compileType]
}

def generateCustomCompileVarString(Map config){
    def customVars=[:]
    def customVarRegex = ~/^(compileVar_).*/
    pipelineLogger.debug("generateCustomCompileVarString method")
    for (var in config){
        def key = var.key
        def val = var.value
        pipelineLogger.debug("key=${key}, val=${val}")
        if( key ==~ customVarRegex ) {
            pipelineLogger.debug("key matches")
            def customVar = key - "compileVar_"
            pipelineLogger.debug("adding vals to map: ${customVar}, ${val}")
            customVars.put(customVar, val)
        }
    }
    pipelineLogger.debug("customVars=${customVars}")
    def customVarString = ""
    for ( x in customVars) {
        def key = x.key
        def val = x.value
        customVarString = "${customVarString} -e '${key}=${val}'"
    }
    pipelineLogger.debug("customVarString=${customVarString}")
    return customVarString
}

def generateCompileVarsAuth(Map config, Map compileVars){
    def compileImage=compileVars["compileImage"]
    def compileImageRegistry=compileImage.split("/")[0]
    compileVars.put("compileImageRegistry", compileImageRegistry)
    def compileRegistryUser=compileVars["compileRegistryUser"]
    def compileRegistryCredsSet=utilities.getDockerPullCredentialsSetName(config,compileImageRegistry,compileRegistryUser)
    compileVars.put("compileRegistryCredsSet", compileRegistryCredsSet)
    return compileVars
}