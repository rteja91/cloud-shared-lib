def runSpecifiedPipelineType(Map config) {
    def serviceNames=utilities.generateServiceNamesListFromConfigMap(config)
    pipelineLogger.info("servicenames= ${serviceNames}")
    def pipelineType = env.pipelineType
    switch(pipelineType) {
        case "compile":
            pipelineTypeWorkflow(config, serviceNames)
        break
        case "deploy":
            pipelineTypeDeploy(config, serviceNames)
        break
        default:
            pipelineLogger.error("Variable 'pipelineType' in .env is not set to any of the expected values ")
            pipelineLogger.error("Expected one of workflow, deploy or custom but found '${env.pipelineType}' ")
    }
}

def call(){
    pipelineLogger.info("Starting Pipeline")
    timestamps {
        def config = [:]
        def customPipeline=""
        
        withFolderProperties{
            echo("setup: ${env.PIPELINE_SETUP_NODE_LABEL}")
            echo("compile: ${env.PIPELINE_COMPILE_NODE_LABEL}")
            echo("deploy: ${env.PIPELINE_DEPLOY_NODE_LABEL}")

        node(env.PIPELINE_SETUP_NODE_LABEL){
            stage('setup') {
                    runGitCheckOut()
                    def customPipelineFileLocation=utilities.getCustomPipelineScriptLocation()
                    if(customPipelineFileLocation != "" ){
                        env.pipelineType="custom"
                        customPipeline=load(customPipelineFileLocation)
                    }else{
                        config = pipelineSetup()
                    }
            }
        }

        pipelineWorkflow(config)

        }
    }
}