def runConfirmPreDeploy(){
    String message="Service is ready to deploy. Please 'Confirm' to proceed "
    stage("Confirm Deployment Start"){
        rfcDetailsInput = input (
            id: "confirmDeploy",
            message: message,
            ok: 'Confirm'
        )
    }
}

def call(Map config, List serviceNames, Map pipelineStagesMap=[:], String nodeLabel=env.PIPELINE_DEPLOY_NODE_LABEL){
    if( pipelineStagesMap["stageDeployFlag"].toBoolean() ){
        Map deployDetails = null
        if ( pipelineStagesMap["stageConfirmDeployFlag"].toBoolean() ){
            runConfirmPreDeploy()
        }
        def deploymentSuccessful=false 
        node(env.PIPELINE_DEPLOY_NODE_LABEL) {
            stage("Deploy Stage"){
                deploymentSuccessful = runDeploy(config, serviceNames)
            }

        }
        if(deploymentSuccessful){
            pipelineLogger.debug("pipelineDeployStages Deployment successful")
            pipelineLogger.info("Deployment was successful")
        }
        if(!deploymentSuccessful){
            pipelineLogger.info("Deployment was not successful")
            error "Deployment was unsuccessful; pipeline exiting with error status"
        }

    }
}