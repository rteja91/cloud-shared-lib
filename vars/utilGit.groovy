#!/usr/bin/groovy
def GetLastCommitFiles() {
    commitHash = sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
    echo "[INFO] Commit is $commitHash"
    echo "[INFO] Branch is '$env.BRANCH_NAME'"
   	filename = sh (script: "git show --name-only", returnStdout: true)
    echo "[INFO] change file name is $filename"
}

def getLastCommitID()
{ 
    echo "DEBUG: Get gitsha1/commmit_id from git repo"
    commit_id = sh(returnStdout: true, script: "git rev-parse HEAD").trim()
    echo "DEBUG: got commit_id: $commit_id"
    return commit_id
}

def setEnvVarsFromGitProperties() {

    // git basic details, required for pipeline
    env.gitCommitHash = sh (script: "git log -n 1 --pretty=format:'%H'", returnStdout: true)
    env.gitUrl = scm.getUserRemoteConfigs()[0].getUrl()-"https://"
    pipelineLogger.info(env.gitUrl)
    env.gitProjectName = gitUrl.split("bitbucket.org/")[1].split("/")[0].toLowerCase()
    env.gitRepoName = gitUrl.tokenize('/').last().split("\\.")[0]
    env.gitBranchName = env.BRANCH_NAME.split("/").last()
    env.gitCommitHashShort=sh (script: "git log -n 1 --pretty=format:'%h'", returnStdout: true)
}

def setAdvancedGitEnvProperties(){

    // Git commit details
    def gitCommitDate = sh (script: "git show --no-patch --no-notes --pretty='%cD' ${env.gitCommitHash} | cut -c1-25", returnStdout: true)
    def gitCommitterName = sh (script: "git show --no-patch --no-notes --pretty='%cn' ${env.gitCommitHash}", returnStdout: true)
  	def gitCommitterSSO = sh (script: "git show --no-patch --no-notes --pretty='%cN' ${env.gitCommitHash}", returnStdout: true)
    def gitCommitterEmail = sh (script: "git show --no-patch --no-notes --pretty='%ce' ${env.gitCommitHash}", returnStdout: true)
    def gitCommitSubject = sh (script: "git show --no-patch --no-notes --pretty='%s' ${env.gitCommitHash}", returnStdout: true)

    env.gitCommitDate = gitCommitDate.trim()
    env.gitCommitterName = gitCommitterName.trim()
  	env.gitCommitterSSO = gitCommitterSSO.trim()
    env.gitCommitterEmail = gitCommitterEmail.trim()
    env.gitCommitSubject = gitCommitSubject.trim()

    // Git URLs
    def gitRemoteUrl = scm.userRemoteConfigs[0].url 					  
   	def gitUrlString = ("${gitRemoteUrl}" =~ /.*(?=\/)/)[0]
  	def gitProjectUrl = "${gitUrlString}".replace("/scm/","/projects/")
    def gitBranchUrl = "${gitProjectUrl}/repos/${env.gitRepoName}/browse?at=${env.gitBranchName}"
	def gitCommitUrl = "${gitProjectUrl}/repos/${env.gitRepoName}/commits/${env.gitCommitHash}"

    env.gitProjectUrl=gitProjectUrl
    env.gitBranchUrl=gitBranchUrl
    env.gitCommitUrl=gitCommitUrl

    // Git history
    def gitDiffStat = sh (script: "git diff --stat ${env.gitCommitHash} ${env.gitCommitHash}~ | sort", returnStdout: true)
    def gitCommitGraph = sh (script: "git log --graph -5 --oneline", returnStdout: true)

    env.gitDiffStat = """$gitDiffStat"""
    env.gitCommitGraph = """$gitCommitGraph"""

    // debug logger
    pipelineLogger.debug("""
        Git variables:

            gitProjectName = $env.gitProjectName
            gitRepoName = $env.gitRepoName
            gitBranchName = $env.gitBranchName
            gitCommitHash = $env.gitCommitHash
            gitProjectUrl = $env.gitProjectUrl

    """) 

}

   /**
    * Check whether the tag already exists in git.  If not, tag the current commit with the tag
    *   Supports the semVer tagging strategy
    * @param tag - map of properties read in from .env
    * @param message - if included, will create an annotated tag using the message
    * @param force - Not yet implemented, included here as a stub.  If set to 'true',
    *                       Will attempt to attach the tag to the current commit hash, even 
    *                       if the tag already exists in the repo.  May require deleting the 
    *                       tag before creating new tag. 
    */
def checkCreateGitTag(String tag, String message="", Boolean force=false) {
    try {
        def credSet=env.SCM_CREDENTIALS_RW
        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "${credSet}", usernameVariable: 'GIT_USERNAME', passwordVariable: 'GIT_PASSWORD']]) {
            pipelineLogger.debug("Fetching tags")
            pipelineLogger.debug("git fetch --tags --force 'https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.gitUrl}'")
            def returnVal = sh script:"git fetch --tags --force 'https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.gitUrl}'  > /dev/null", returnStatus:true
            assert returnVal == 0: "Failed to fetch tags - possibly invalid credentials?"

            returnVal = sh script:"git rev-parse '${tag}' > /dev/null", returnStatus:true
            if ( returnVal != 0 || force ) {
                //Tag does not exist in remote repo yet
                pipelineLogger.info("Tag '${tag}' does not yet exist in Bitbucket.  Attempting to create it...")
                String annotatedTagString=""
                if ( message != "" ) {
                    annotatedTagString = "-a -m \"${message}\""
                } 
                String forceFlag=""
                if (force) {
                    forceFlag="-f"
                }
                returnVal = sh script: "git tag ${forceFlag} ${annotatedTagString} ${tag} ", returnStatus:true
                assert returnVal == 0 : "Failed to create tag"
                pipelineLogger.debug("Pushing tag with command: 'git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.gitUrl} ${tag}'")
                returnVal =sh script: "git push https://${GIT_USERNAME}:${GIT_PASSWORD}@${env.gitUrl} ${tag} > /dev/null 2>&1", returnStatus:true
                assert returnVal == 0 : "Failed to push tag"
                pipelineLogger.info("Successfully tagged commit ${env.gitCommitHash} as '${tag}'")
            }
            else {
                pipelineLogger.error("Found tag '${tag}' already existing in bitbucket.  To force creating the tag, include the argument 'force=true' in the method call.")
            }
        }
    }
    catch(Exception e){
        pipelineLogger("Error while creating or pushing tag '${tag}'", "ERROR")
        sh script:"git tag -d ${tag} > /dev/null 2>&1", returnStatus:true
        pipelineLogger.error("Caught Exception, printing Stack Trace: ${e}")
    }
}
