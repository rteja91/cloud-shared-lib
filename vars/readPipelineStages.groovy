def call(Map config){
    
    def pipelineStagesMap = [:]
    def stageCompileEnabled = config["stageCompile"]
    def stageSecurityScansEnabled = config["stageSecurityScans"]
    def stageDockerBuildEnabled = config["stageDockerBuild"]
    def stageDockerPushEnabled = config["stageDockerPush"]
    def stageBackoutEnabled = config["backoutRequired"]
    def stageConfirmDeploy=config["confirmDeploy"]
    def stageDeploy=config["stageDeploy"]
    def stageDownStreamJobsEnabled=config["workflowStages"]

    pipelineStagesMap.put("stageCompileFlag",false)
    pipelineStagesMap.put("stageSecurityScansFlag",false)
    pipelineStagesMap.put("stageDockerBuildFlag",false)
    pipelineStagesMap.put("stageDockerPushFlag",false)
    pipelineStagesMap.put("stageBackoutFlag",false)
    pipelineStagesMap.put("stageConfirmDeployFlag",false)
    pipelineStagesMap.put("stageDeployFlag",false)
    pipelineStagesMap.put("stageTriggerDownStreamJobs",false)

    if( stageCompileEnabled != null && stageCompileEnabled == "true" ){
        pipelineStagesMap.put("stageCompileFlag",true)
    }
    if( stageSecurityScansEnabled != null && stageSecurityScansEnabled == "true" ){
        pipelineStagesMap.put("stageSecurityScansFlag",true)
    }
    if( stageDockerBuildEnabled != null && stageDockerBuildEnabled == "true" ){
        pipelineStagesMap.put("stageDockerBuildFlag",true)
    }
    if( stageDockerPushEnabled != null && stageDockerPushEnabled == "true" ){
        pipelineStagesMap.put("stageDockerPushFlag",true)
    }
    if( stageBackoutEnabled != null && stageBackoutEnabled == "true" ){
        pipelineStagesMap.put("stageBackoutFlag",true)
    }
    if(  stageConfirmDeploy != null && stageConfirmDeploy == "true" ){
        pipelineStagesMap.put("stageConfirmDeployFlag",true)
    }
    if( stageDeploy != null && stageDeploy == "true" ){
        pipelineStagesMap.put("stageDeployFlag",true)
    }
    if( stageDownStreamJobsEnabled != null && stageDownStreamJobsEnabled != "" ){
        pipelineStagesMap.put("stageTriggerDownStreamJobs",true)
    }
    
    return pipelineStagesMap
    
}