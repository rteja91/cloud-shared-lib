  /**
    * Parses and creates build args from properties and other sources for each service
    * Combines:
    *   -Custom values defined in properties like:
    *       <service-name>_BUILD_ARG_<arg-name>=<arg-value>
    *       which will be converted into a build arg for <service-name> like:
    *       --build-arg <arg-name>=<arg-value>
    *   -default build args, set to the corresponding values:
    *       -COMMIT_HASH
    *       -TAG
    *       -GIT_BRANCH_NAME
    *   -Parameters - Can be custom defined, or defaults below:
    *       -UPSTREAM_IMAGE_TAG
    *       -UPSTREAM_IMAGE_BRANCH
    *
    * @param config - Map of properties read in from properties file
    * @param serviceNames - List of service names parsed from config
    * @return serviceNameToBuildArgsStringMap - Map with the following structure:
    *   ["serviceName":"--build-arg UPSTREAM_IMAGE_TAG=1.0.0 --build-arg UPSTREAM_IMAGE_BRANCH=develop"]
    */
def generateBuildArgsMap(Map config, List serviceNames, Map upstreamRegistryPropertiesMap) {
    def BUILD_ARG_PATTERN='_BUILD_ARG_'
    def BUILD_SECRET_PATTERN='_BUILD_SECRET_'
    def serviceNameToBuildArgsMapMap = [:]
    def serviceNameToBuildSecretsMapMap = [:]

    for ( envVar in config) {
        def envVarName=envVar.key
        def envVarValue=envVar.value
        if (envVarName.contains(BUILD_ARG_PATTERN)) {
            def buildArgsSplit=envVarName.split(BUILD_ARG_PATTERN)
            def buildArgsMap=[:]
            def serviceName=buildArgsSplit[0]
            if (serviceNameToBuildArgsMapMap.containsKey(serviceName)) {
                buildArgsMap=serviceNameToBuildArgsMapMap.get(serviceName)
            }
            buildArgsMap.put(buildArgsSplit[1], envVarValue)
            serviceNameToBuildArgsMapMap.put(serviceName, buildArgsMap)
        }
        else if (envVarName.contains(BUILD_SECRET_PATTERN)) {
            def buildSecretsSplit=envVarName.split(BUILD_SECRET_PATTERN)
            def buildSecretsMap=[:]
            def serviceName=buildArgsSplit[0]
            if (serviceNameToBuildSecretsMapMap.containsKey(serviceName)) {
                buildSecretsMap=serviceNameToBuildSecretsMapMap.get(serviceName)
            }
            buildSecretsMap.put(buildSecretsSplit[1], envVarValue)
            serviceNameToBuildSecretsMapMap.put(serviceName, buildSecretsMap)
        }
    }
    serviceNameToBuildArgsStringMap =[:]
    for (serviceName in serviceNames) {
        def buildArgsMap=[:]
        if (serviceNameToBuildArgsMapMap.containsKey(serviceName)) {
            buildArgsMap = serviceNameToBuildArgsMapMap.get(serviceName)
            //echo "buildArgsMap=${buildArgsMap}"
        }
        
        //Add parameters as build args, but don't overwrite values already defined in .env
        //eg. param sets UPSTREAM_IMAGE_TAG as 3.2.0 but service2 image is building from a different 
        // base and has 1.0.0 as the upstream tag.  
        //In that case, either set service2_BUILD_ARG_UPSTREAM_IMAGE_TAG=1.0.0, or set
        // customParameterList = "service2UpstreamVersion"
        //parameterDefaultValue_service2UpstreamVersion=1.0.0
        //parameterDescription_service2UpstreamVersion="this is the upstream version for service2"
        pipelineLogger.debug("Grabbing build args from params")
        for (key in params.keySet()) {
            def paramName=key
            def paramValue = params[key]
            if ( paramName != "INVOCATION_COMMAND" && ! buildArgsMap.containsKey(paramName) ) {
                    //These values are likely used in the FROM section of dockerfile and it will only work with lower case values
                if ( paramName == "UPSTREAM_IMAGE_BRANCH" ) {
                    pipelineLogger.debug("Changing value of 'UPSTREAM_IMAGE_BRANCH' for service '${serviceName}' from '${paramValue}' to '${paramValue.toLowerCase()}'")
                    paramValue=paramValue.toLowerCase()
                }
                buildArgsMap.put(paramName, paramValue)
                
            }
        }

        //Add few build args for all builds
        //Commit hash this image was built from
        buildArgsMap.put("COMMIT_HASH", env.gitCommitHash)
        //Branch this image was built from
        buildArgsMap.put("GIT_BRANCH_NAME", env.gitBranchName)
        //Tag this image is tagged with
        buildArgsMap.put("TAG", env.tag)
        if ( serviceName == "invocation" ){
            buildArgsMap.put("DOCKER_REGISTRY_URL", runDockerPush.getDockerPushUrl(config))
            buildArgsMap.put("BITBUCKET_PROJECT_KEY", utilities.generateProjectNameVar(config))
            buildArgsMap.put("BITBUCKET_REPO_NAME", env.gitRepoName)
            buildArgsMap.put("DOCKER_IMAGE_NAME_PREFIX", utilities.generateDockerImagePrefix(config))

            def serviceNamesString=""
            for ( name in serviceNames ){
                if ( name != "invocation"){
                    if ( serviceNamesString == "" ){
                        serviceNamesString = name
                    }
                    else {
                        serviceNamesString = "${serviceNamesString}, ${name}"
                    }
                }
            }

            buildArgsMap.put("SERVICE_NAMES", serviceNamesString)
        }

        //Figure out the upstream repo url for the image and pass that as a build arg
        if ( ! buildArgsMap.containsKey("UPSTREAM_REGISTRY_URL")) {
            def upstreamRepoConfigString="${serviceName}_upstreamRegistry"
            def upstreamRepoName = config[upstreamRepoConfigString]
            if ( upstreamRepoName == null || upstreamRepoName == "" ){
                upstreamRepoName = "default"
            }
            upstreamRepoUrl = upstreamRegistryPropertiesMap[upstreamRepoName]["url"]
            buildArgsMap.put("UPSTREAM_REGISTRY_URL", upstreamRepoUrl)
        }

        def buildArgsString=""
        for ( buildArgMapping in buildArgsMap) {
            buildArgsString="${buildArgsString} --build-arg \'${buildArgMapping.key}=${buildArgMapping.value}\'"
        }
        serviceNameToBuildArgsStringMap.put(serviceName,buildArgsString)
    }    

    return serviceNameToBuildArgsStringMap
}

  /**
    * Parses and creates default labels from properties for each service
    * 
    * 
    *
    * @param config - Map of properties read in from properties file
    * @param serviceNames - List of service names parsed from config
    * @return serviceNameToBuildLabelsStringMap - Map with the following structure:
    *   ["serviceName":"--label <docker-image-name>_COMMIT_HASH=<commit-hash> --label <label2>=<value2>"]
    */
def generateBuildLabelsMap(Map config, List serviceNames) {
    def BUILD_LABEL_PATTERN='_BUILD_LABEL_'
    serviceNameToBuildLabelsMap = [:]
    for ( envVar in config) {
        def envVarName=envVar.key
        def envVarValue=envVar.value
        if (envVarName.contains(BUILD_LABEL_PATTERN)) {
            def buildArgsSplit=envVarName.split(BUILD_LABEL_PATTERN)
            def buildArgLabels=[:]
            def serviceName=buildArgsSplit[0]
            if (serviceNameToBuildLabelsMap.containsKey(serviceName)) {
                buildArgLabels=serviceNameToBuildLabelsMap.get(serviceName)
            }
            buildArgLabels.put(buildArgsSplit[1], envVar.value)
            serviceNameToBuildLabelsMap.put(serviceName, buildArgLabels)
        }
    }

    serviceNameToBuildLabelsStringMap =[:]
    for (serviceName in serviceNames) {
        def buildLabelsMap=[:]
        if (serviceNameToBuildLabelsMap.containsKey(serviceName)) {
            buildLabelsMap = serviceNameToBuildLabelsMap.get(serviceName)
        }
        //Add few labels for all builds
        buildLabelsMap.put("${utilities.generateDockerImageName(serviceName, config)}_COMMIT_HASH", env.gitCommitHash)
        buildLabelsMap.put("${utilities.generateDockerImageName(serviceName, config)}_REPO_URL", env.gitUrl)

        def buildLabelsString=""
        for ( buildLabel in buildLabelsMap) {
            buildLabelsString="${buildLabelsString} --label '${buildLabel.key}=${buildLabel.value}'"
        }
        serviceNameToBuildLabelsStringMap.put(serviceName,buildLabelsString)
    }    
    return serviceNameToBuildLabelsStringMap
}


def getUpstreamRegistryNamesList(config) {
    pipelineLogger.debug("Entered getUpstreamRegistryNamesList")
    def final customRegistryTypeSuffix="_upstreamRegistryType"

    def upstreamRegistryNamesList = [] 

    for ( envVarName in config.keySet()) {
        if ( envVarName.endsWith(customRegistryTypeSuffix) ) {
            def upstreamRegistryName=envVarName.split(customRegistryTypeSuffix)[0]
            upstreamRegistryNamesList.add(upstreamRegistryName)
        }
    }

    return upstreamRegistryNamesList
}

def getRegistryPropertiesAws(Map config, String upstreamRegistryName) {
    def UPSTREAM_REGISTRY_ACCOUNT_NUMBER_SUFFIX="_upstreamRegistryAccountNumber"
    def UPSTREAM_REGISTRY_REGION_SUFFIX="_upstreamRegistryRegion"
    def UPSTREAM_REGISTRY_USER_SUFFIX="_upstreamRegistryUser"
    def registryProperties = [:]

    def accountNumberConfigString="${upstreamRegistryName}${UPSTREAM_REGISTRY_ACCOUNT_NUMBER_SUFFIX}"
    def accountNumber = config[accountNumberConfigString]
    def regionConfigString="${upstreamRegistryName}${UPSTREAM_REGISTRY_REGION_SUFFIX}"
    def region = config[regionConfigString]
    def userConfigString="${upstreamRegistryName}${UPSTREAM_REGISTRY_USER_SUFFIX}"
    def user = config[userConfigString]
    
    //if properties are not defined for this repo, use the main aws account properties 
    if ( accountNumber == null || accountNumber == "" ) {
        accountNumber=config["awsAccountNumber"]
    }
    if ( region == null || region == "" ) {
        region=config["awsRegion"]

    }
    if ( user == null || user == "" ) {
        user=config["awsUser"]
    }

    assert accountNumber != null && accountNumber != "" : "Error: ${upstreamRegistryName}${UPSTREAM_REGISTRY_ACCOUNT_NUMBER_SUFFIX} is not set, and awsAccountNumber is also not set.  Unable to get properties for this Docker registry."
    assert accountNumber != null && accountNumber != "" : "Error: ${upstreamRegistryName}${UPSTREAM_REGISTRY_REGION_SUFFIX} is not set, and awsRegion is also not set.  Unable to get properties for this Docker registry."
    assert accountNumber != null && accountNumber != "" : "Error: ${upstreamRegistryName}${UPSTREAM_REGISTRY_USER_SUFFIX} is not set, and awsUser is also not set.  Unable to get properties for this Docker registry."

    def url = "${accountNumber}.dkr.ecr.${region}.amazonaws.com"
    def creds = "ecr:${region}:${user}"
    registryProperties.put("url", url)
    registryProperties.put("creds", creds)
    pipelineLogger.debug("Upstream Docker repo name: '${upstreamRegistryName}' has url '${url}' and creds '${creds}'")
    return registryProperties
}

def getRegistryPropertiesPrivate(Map config, String upstreamRegistryName) {
    def UPSTREAM_REGISTRY_URL_SUFFIX="_upstreamRegistryUrl"
    def registryProperties = [:]
    def registryUrlToCredsSetMap=[
        'index.docker.io':"${env.DOCKER_REG_CRED}",
        'docker.io':"${env.DOCKER_REG_CRED}",
        'registry-1.docker.io':"${env.DOCKER_REG_CRED}"
    ]

    //def urlConfigString="${upstreamRegistryName}${UPSTREAM_REGISTRY_URL_SUFFIX}"
    //def url= config[urlConfigString]
    def url= config[upstreamRegistryName+UPSTREAM_REGISTRY_URL_SUFFIX]
    def creds=""

    assert registryUrlToCredsSetMap.containsKey(url) : "ERROR: Could not find upstreamRegistryUrl '${url}' in the possible values '${registryUrlToCredsSetMap.keySet()}'"

    creds=registryUrlToCredsSetMap[url]

    registryProperties.put("url", url)
    registryProperties.put("creds", creds)
    pipelineLogger.debug("Upstream Docker repo name: '${upstreamRegistryName}' has url '${url}' and creds '${creds}'")

    return registryProperties

}

def getRegistryNameToServiceNamesMap(Map config, List upstreamRegistryNamesList, List serviceNames) {
    /*
    cms_upstreamRegistry=aws-c1
    site_upstreamRegistry=aws-c1
    luceneexport_upstreamRegistry=aws-c1
    invocation_upstreamRegistry=dockerdev
    */
    def UPSTREAM_REGISTRY_SUFFIX_STRING="_upstreamRegistry"
    def registryNameToServiceNamesMap = [:]
    def tempServiceNamesList = serviceNames

    def defaultRegistryDefined = false
    if ( upstreamRegistryNamesList.contains("default") ){
        defaultRegistryDefined = true
    }


    for ( serviceName in serviceNames ){
        //def registryNameConfigString="${serviceName}${UPSTREAM_REGISTRY_SUFFIX_STRING}"
        def registryName = config[serviceName+UPSTREAM_REGISTRY_SUFFIX_STRING]

        assert ( ( defaultRegistryDefined && ( registryName == null || registryName == "" ) ) || upstreamRegistryNamesList.contains(registryName)  )  : "ERROR: Either no default registry is specified, or a service refers to a registry that is not defined.  Pipeline will exit."


        if ( registryName == null || registryName == "" ) {
            pipelineLogger.debug("${serviceName+UPSTREAM_REGISTRY_SUFFIX_STRING} not found, assuming default registry")
            registryName="default"
        }

        if ( upstreamRegistryNamesList.contains(registryName) ) {
            def servicesInRegistry=[]

            if ( registryNameToServiceNamesMap.containsKey(registryName) ) {
                servicesInRegistry = registryNameToServiceNamesMap[registryName]
            }

            servicesInRegistry.add(serviceName)
            registryNameToServiceNamesMap.put(registryName, servicesInRegistry)
        }
        pipelineLogger.debug("service '${serviceName}' found to have upstream registry '${registryName}'")
    }
    return registryNameToServiceNamesMap

}

def getRegistryPropertiesMap(Map config, List upstreamRegistryNamesList) {
    def final customRegistryTypeSuffix="_upstreamRegistryType"
    pipelineLogger.debug("Entered getRegistryPropertiesMap")

    def upstreamRegistries = [:]
    for ( upstreamRegistryName in upstreamRegistryNamesList ) {
        pipelineLogger.debug("Checking config for ${upstreamRegistryName}${customRegistryTypeSuffix}")
        //def upstreamRegistryConfigString = "${upstreamRegistryName}${customRegistryTypeSuffix}"
        def upstreamRegistryType = config[upstreamRegistryName+customRegistryTypeSuffix]

        def registryProperties = [:]
        if ( upstreamRegistryType ==  null || upstreamRegistryType == "" ) {
            //Error
            pipelineLogger.error("Upstream registry defined but type not defined")
        }
        else if ( upstreamRegistryType == "aws") {
            registryProperties = getRegistryPropertiesAws(config, upstreamRegistryName)
        }
        else if ( upstreamRegistryType == "private") {
            registryProperties = getRegistryPropertiesPrivate(config, upstreamRegistryName)
        }

        upstreamRegistries.put(upstreamRegistryName, registryProperties)
    }

    return upstreamRegistries
}


  /**
    * Builds docker images for all defined services
    *  -Parses and adds default build args from properties and passes then to the docker build command
    *  -Parses and adds default labels from properties and passes then to the docker build command
    *
    * Currently supports building when all upstream images are in the same registry.  If required, could
    *   refactor to support building each image from different upstream registry.
    *
    * @param config - Map of properties read in from properties file
    * @param serviceNames - List of service names parsed from config
    * @customRegistery - if app team dont care about registry pass false. this is simpler way and no need to maintain upstream and nothing needs to be defined at .env
    */
def call(Map config, List serviceNames =[] , boolean customRegistery = true) {

    serviceNames = (serviceNames.isEmpty()) ? utilities.generateServiceNamesListFromConfigMap(config) : serviceNames

    pipelineLogger.info("Entering Docker Build Stage")
    assert ! serviceNames.isEmpty() : "ERROR: No services have been defined.  Docker Build stage requires serviceNames to be defined." 
    
    if (customRegistery)
    {
        def upstreamRegistryNamesList=getUpstreamRegistryNamesList(config)
        assert ! upstreamRegistryNamesList.isEmpty() : "ERROR: No upstream registries have been defined.  Pipeline will exit." 

        def upstreamRegistryPropertiesMap=getRegistryPropertiesMap(config, upstreamRegistryNamesList)
        pipelineLogger.debug("upstreamRegistryPropertiesMap=${upstreamRegistryPropertiesMap}")
        def registryNameToServiceNameMap=getRegistryNameToServiceNamesMap(config, upstreamRegistryNamesList, serviceNames)

        def serviceNameToBuildArgsStringMap=generateBuildArgsMap(config, serviceNames, upstreamRegistryPropertiesMap)
        def serviceNameToImageLabelsStringMap=generateBuildLabelsMap(config, serviceNames)
        


        for ( registryName in upstreamRegistryNamesList ) {
            if (registryNameToServiceNameMap.containsKey(registryName)) {

                def registryProperties = upstreamRegistryPropertiesMap[registryName]
                def upstreamDockerRegistryUrl = registryProperties["url"]
                def upstreamDockerRegistryCreds = registryProperties["creds"]

                

                docker.withRegistry("https://${upstreamDockerRegistryUrl}", upstreamDockerRegistryCreds) {
                    for (serviceName in registryNameToServiceNameMap[registryName] ) {
                        def imageName=utilities.generateDockerImageName(serviceName, config)
                        def imageNameWithTag="${imageName}:${env.tag}"
                        pipelineLogger.debug("generated imageNameWithTag=${imageNameWithTag}")
                        /*if ( config.containsKey("tag") && config["tag"] != "") {
                            imageName+=":"+config["tag"]
                        }
                        */
                        def buildArgsString=serviceNameToBuildArgsStringMap[serviceName]
                        def buildLabelsString=serviceNameToImageLabelsStringMap[serviceName]
                        //def upstreamRegistry=serviceNameToUpstreamImageRegistryMap[serviceName]
                        //TODO future enhancement -> modify this to allow different upstream registry for each service
                        //See generateUpstreamImageNameToRegistryMap function
                        pipelineLogger("Building image for ${serviceName}")
                        pipelineLogger.debug("buildArgsString=${buildArgsString}")
                        pipelineLogger.debug("buildLabelsString=${buildLabelsString}")
                        
                        def dockerFilePath = (config["customDockerImagesPath"] == null || config["customDockerImagesPath"] == "") ? "docker-images/${serviceName}/Dockerfile": config["customDockerImagesPath"]
           
                        pipelineLogger.debug("dockerFilePath=${dockerFilePath}")
                        image = docker.build(imageNameWithTag, "--pull ${buildArgsString} ${buildLabelsString} -f ${dockerFilePath} .")
                        
                        pipelineLogger("Successfully built docker image ${imageNameWithTag}")
                    }
                }

            }
        }
    }
    else
    {
        // simplest way to build docker continers. 
       pipelineLogger("entering simplest docker build logic.") 
       config.put('customRegistery',false)
       build(config,serviceNames)
    }



    
}

  /**
    * Wrapper around 'stageDockerBuild' call method to run it inside a stage
    *
    * @param config - Map of properties read in from properties file
    * @param serviceNames - List of service names parsed from config
    */
def runStage(Map config, List serviceNames=[]) {
    stage ('Docker Build') {
        script {
            //GS: need to confirm this logic from stefan , if we are readign name frm config why to pass as seperate param. Makeing is generic so that it doesnt break
            def service_names = (serviceNames.isEmpty()) ? utilities.generateServiceNamesListFromConfigMap(config) : serviceNames
            runDockerBuild(config,service_names)
        }
    }
}

def getDockerRegistry(String releaseBranch){
    def dockerRegistry = ""
   	def prodBranch = (releaseBranch == null) ? "${env.PROD_BRANCH_NAME}": releaseBranch
    dockerRegistry = ("${env.BRANCH_NAME}" == "${prodBranch}") ? "${env.dockerPushReleaseRegistryUrl}" : "${env.dockerPushDevRegistryUrl}"
    pipelineLogger.debug("Docker Build registry is set as ${dockerRegistry}")
    return dockerRegistry
}

/***
 * Builds docker image for the dockerfile present in specified directory
 * @param dockerDirName
 * @return
 */
def build(Map config, List serviceNames=[]) {
   def service_names = (serviceNames.isEmpty()) ? utilities.generateServiceNamesListFromConfigMap(config) : serviceNames
   pipelineLogger.info("Building docker images.")
    for ( serviceName in serviceNames )
    {
        def dockerfilePath = "docker-images/${serviceName}/Dockerfile"
        if (!fileExists("${dockerfilePath}"))
        {
            pipelineLogger.fatal("No service folder found")
        }   

        def dockerBuildRegistry = getDockerRegistry(config['releaseBranch'])

        //Login to both dev & release registries, since we may require to pull image from both registries during development phase
        //Used WithCredentials for Docker login due to known issue with docker.withRegistry for nested logins(https://issues.jenkins-ci.org/browse/JENKINS-59777)
        def credSet = env.JENKINS_CREDENTIALS_RO
        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "${credSet}", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']])
        {
            // login to both the registry "${env.dockerPushReleaseRegistryUrl}" : "${env.dockerPushDevRegistryUrl}"
            sh "docker login ${env.dockerPushReleaseRegistryUrl} -u ${USERNAME} -p ${PASSWORD}"
            sh "docker login ${env.dockerPushDevRegistryUrl} -u ${USERNAME} -p ${PASSWORD}"
            def imageName=utilities.generateDockerImageName(serviceName, config)
            def imageNameWithTag="${imageName}:${env.tag}"
            pipelineLogger.debug("generated imageNameWithTag=${imageNameWithTag}")
            pipelineLogger.info("Building ${serviceName}.")
            pipelineLogger.debug("Docker Image Name : ${imageName}")
            image = docker.build(imageNameWithTag, " --build-arg ARTIFACTORY_USERNAME=${USERNAME} --build-arg ARTIFACTORY_PASSWORD=${PASSWORD}  --tag ${imageName}:latest -f ${dockerfilePath} .")
            pipelineLogger.info("Successfully built docker image ${imageName}")
        }
    
    }
}
