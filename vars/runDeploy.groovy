  /**
    * Run invocation image with 'install' argument, or custom argument if passed from .env
    *  Requires withRegistry block so we can use fully qualified image name for invocation
    *   i.e. <registry>/<image-name>:<tag>
    *   This is required so we can make sure to run the correct image even if another image has already 
    *   been built but not tagged.  This also supports backout, which updates 'env.tag' to be the backout
    *   tag - that image may not be present on the host yet so it would need to login to the registry.
    *  If "invocationCommand" is defined in properties, that command will be passed to run script.  Otherwise it will use default command "install"
    *
    * @param config - map of properties read in from properties file
    * @param serviceNames - list of service names parsed from properties file
    */
def call(Map config, List serviceNames, Boolean dryRun=false) {
    pipelineLogger.debug("Entered Deploy Stage")

    if ( ! serviceNames.contains("invocation")) {
        pipelineLogger.error("Deployment stage started, but no service named 'invocation' has been defined.  Nothing will be deployed.")
        error
    }

    String deployTarget = config['deployTarget']
    if ( deployTarget == null ) {
        deployTarget = ""
    }

    String deployUser = config["deployUser"]
    if ( deployUser == null ){
        deployUser = ""
    }

    if ( deployTarget == "dcos" && deployUser == "" ){
        deployUser = calculateDeployUserDCOS(config)
        pipelineLogger.info("Parsed deployTarget as 'dcos' and calculated deployUser as ${deployUser}")
    }
    else if ( config.containsKey("awsUser")  && config["awsUser"] != "" ) {
        deployUser = config["awsUser"]
    } 
    if ( deployTarget ){
        pipelineLogger.debug("deployTarget=${deployTarget}")
    }
    pipelineLogger.debug("deployUser=${deployUser}")

    String invocationImageName = utilities.generateDockerImageName("invocation", config)
    String invocationImageTag = env.tag

    String registryUrl
    String registryCredSetName
    (registryUrl, registryCredSetName) = runDockerPush.getRegistryProperties(config)

    String fullImageName=registryUrl+"/"+invocationImageName+":"+invocationImageTag
    pipelineLogger.debug("fullImageName=${fullImageName}")

    String sanitizedContainerName=fullImageName.replaceAll("/", "-").replaceAll(":", "-")
    def containerNameString=" --name ${sanitizedContainerName} "

    def invocationCommand=params.INVOCATION_COMMAND

//First, check if 'deployTarget' is set to 'dcos' and 'deployUser' is not set - in that case, run 'calculateDeployUserDCOS' to set deployUser

//search for credentials as AWS creds
//if found, continue with aws deploy logic
//if not found, search for creds as regular (username/password) type
//if found, continue with standard deploy logic

//could allow multiple credentials by using recursion and labeling each credential set like "USER1_AWS_ACCESS_KEY_ID"
//UPDATE: withCredentials should accept a list of multiple credentials, no need for recursion
    def pipelineLogs=null
    if (! checkCredentialsAvailable(deployUser)){
        pipelineLogger.error("The requested credentials - '${deployUser}' are not found.  Are they setup for the folder where the job is running?")
        error
    }

    String credentialType=getCredentialType(deployUser)    
    List credsList=getCredsList(deployUser, credentialType)

    int invocationReturnVal=null
    withCredentials(credsList){
        pipelineLogger.info("Got credentials for ${deployUser}!")
        String deploymentCredsString = generateDeploymentCredsString(credentialType, config)

        try{
            if ( ! dryRun ){
                docker.withRegistry("https://${registryUrl}", registryCredSetName) {

                    invocationReturnVal = sh script: "docker run -i ${containerNameString} ${deploymentCredsString} ${fullImageName} ${invocationCommand}", returnStatus: true
                }
//TODO: write output of invocation logs into a file so it would be available to check later 

//  Groovy has a 64k limit on String size.  It is possible logs will be longer than that, so we should not try to store all logs into a String.  Instead put them into a file somewhere.
//  This line is an example but not tested:                  
//                    sh script: "rm -rf /tmp/invocation-logs/${containerNameString}.txt && docker logs ${containerNameString} > /tmp/invocation-logs/${containerNameString}.txt"
                
                pipelineLogger.debug("Invocation returned status of '${invocationReturnVal}'")
                
            }
        }
        catch(Exception e){
            pipelineLogger.error("Caught exception while attempting to run invocation image.  Marking return status as 1 (failure)")
            invocationReturnVal=1
        }
    }
    Boolean deploymentSuccessful=false
    if ( invocationReturnVal == 0 ){
        deploymentSuccessful=true
    }
    pipelineLogger.debug("Deploy stage completed, returning status as '${deploymentSuccessful}'")
    
        
    return deploymentSuccessful

}


def generateDeploymentCredsString(String credentialType, Map config, String oldDeploymentCredsString="") {
    String deploymentCredsString=oldDeploymentCredsString
    if ( credentialType == "aws" ) {
        String awsRegion = utilities.checkConfigVarExistsNotEmpty(config, "awsRegion")
        deploymentCredsString+="-e 'AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}' -e 'AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}' -e 'AWS_DEFAULT_REGION=${awsRegion}' "
    }
    else if ( credentialType == "userPass" ){
        deploymentCredsString+="-e 'DEPLOY_USERNAME=${DEPLOY_USERNAME}' -e 'DEPLOY_PASSWORD=${DEPLOY_PASSWORD}' "
    }
    else {
        pipelineLogger.error("Unrecognized credentialType '${credentialType}'. Unable to generate deployment credential string")
    }
    return deploymentCredsString
}

def getCredsList(String credentialId, String credentialType=null) {
    List credsList=[]

    if ( credentialType == null ){
        credentialType=getCredentialType(credentialId)
    }
    if ( credentialType == "aws" ) {
        credsList.add([$class: 'AmazonWebServicesCredentialsBinding', credentialsId: credentialId])
    }
    else {
        credsList.add([$class: 'UsernamePasswordMultiBinding', credentialsId: credentialId, usernameVariable: 'DEPLOY_USERNAME', passwordVariable: 'DEPLOY_PASSWORD'])
    }
    return credsList
}


def checkValidAwsCreds(String credentialId) {
    Boolean awsCreds=false
    try {
        List credsList=[]
        credsList.add([$class: 'AmazonWebServicesCredentialsBinding', credentialsId: credentialId])
        withCredentials(credsList){
            pipelineLogger.debug("Credentials were successfully grabbed as type AWS")
        }
        awsCreds=true
    }
    catch(Exception e) {
        pipelineLogger.debug("Caught exception checking whether credentials are type aws.  These are not AWS creds.")
    }
    return awsCreds
}

def checkValidUsernamePasswordCreds(String credentialId) {
    Boolean usernamePasswordCreds=false
    try {
        List credsList=[]
//        credsList.add([$class: 'UsernamePasswordMultiBinding', credentialsId: credentialId, usernameVariable: 'DEPLOY_USERNAME', passwordVariable: 'DEPLOY_PASSWORD'])
        credsList.add([$class: 'UsernamePasswordMultiBinding', credentialsId: credentialId])
        withCredentials(credsList){
            pipelineLogger.debug("Credentials were successfully grabbed as type Username/Password")
        }
        usernamePasswordCreds=true
    }
    catch(Exception e) {
        pipelineLogger.debug("Caught exception checking whether credentials are type Username/Password.  These are not Username/Password creds.")
    }
    return usernamePasswordCreds
}

def checkCredentialsAvailable(String credentialId){
    if ( getCredentialType(credentialId) == null ) {
        return false
    }
    return true
}

def getCredentialType(String credentialId){
    String credentialType=null
    if ( checkValidAwsCreds(credentialId)) {
        credentialType="aws"
    }
    else if ( checkValidUsernamePasswordCreds(credentialId)){
        credentialType="userPass"
    }
    return credentialType
}


def runDeployHelper(){
    pipelineLogger.info("runDeployHelper")
}

def calculateDeployUserDCOS(Map config){
    def deployEnv = config["deployEnv"]
    def deployUser=""    
    assert ! ( deployEnv == null || deployEnv == "" ) : "Error: 'deployTarget' has been specified as 'dcos', but neither 'deployEnv' nor 'deployUser' have been set.\nPossible values for deployEnv are 'E2' or 'E3'"
    
    assert (  ["e2", "e3"].contains(deployEnv.toLowerCase())  ) : "Invalid setting for 'deployEnv'.  Valid options are 'E2' or 'E3'"
    if ( deployEnv.toLowerCase() == "e2" ) { 
        //Check the correct naming of these vars
        deployUser = env.DCOS_E2_DEPLOY_USER
    }
    else if ( deployEnv.toLowerCase() == "e3" ) {
        //Check the correct naming of these vars
        deployUser = env.DCOS_E3_DEPLOY_USER            
    }
    assert deployUser != "" : "Attempting to calculate deployUser for DCOS failed"
    return deployUser
}

  /**
    * Wrapper around 'stageDeploy' call method to run it inside a stage
    *
    * @param config - Map of properties read in from properties file
    * @param serviceNames - List of service names parsed from config
    */
def runStage(Map config, List serviceNames) {
    stage ('Deploy') {
        script{
            runDeploy(config,serviceNames)
        }
    }
}
