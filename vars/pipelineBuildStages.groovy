def runStages(Map config, List serviceNames, Integer numRetries, Map pipelineStagesMap) {
    def stageNameSuffix=""
    if ( numRetries > 0 ){
        stageNameSuffix="- Retry ${numRetries} "
    }

    def isProduction=env.isProduction.toBoolean()
    def stageCompileFlag=pipelineStagesMap["stageCompileFlag"].toBoolean()
    def stageDockerBuildFlag=pipelineStagesMap["stageDockerBuildFlag"].toBoolean()
    def stageDockerPushFlag=pipelineStagesMap["stageDockerPushFlag"].toBoolean()
    
    stage("Checkout Code ${stageNameSuffix}"){
        runGitCheckOut(false)
    }
    
    if(stageCompileFlag){
        stage("Compile${stageNameSuffix}"){
            runCompileInDocker(config)
        }
    }

    if(stageDockerBuildFlag){
        stage("Docker Build ${stageNameSuffix}"){
            runDockerBuild(config, serviceNames)
        }
    }

    if (stageDockerPushFlag) {
        stage(" Docker Push${stageNameSuffix}" ){
            runDockerPush(config, serviceNames)
        }
    } 
    else if ( stageDockerBuildFlag ){
        pipelineLogger.warn("Docker build stage is not enabled, but not Docker push. to enable Docker push, add 'stageDockerPush=true' in properties file")
    }

}




def call(Map config, List serviceNames, Map pipelineStagesMap=[:], String nodeLabel=env.PIPELINE_COMPILE_NODE_LABEL ){
    def retriesEnabled=config["retriesEnabled"]
    def maxRetries=config["maxRetries"]
    if(retriesEnabled !=null && retriesEnabled ){
        if( maxRetries == null ){
            maxRetries = 2
        }
    }
    def numRetries=0
    def tryAgain=true
    while ( tryAgain ){
        try {
            node(nodeLabel){
                try {
                    runStages(config, serviceNames, numRetries, pipelineStagesMap)

                    tryAgain=false
                }
                catch (Exception e){
                    pipelineLogger.error("Caught Exception, printing stack trace: ${e} ")
                    throw e
                }
                finally {
                    utilities.cleanup()
                }
            }
        }
        catch(Exception e){
            if( numRetries >= maxRetries || !retriesEnabled ){
                if(numRetries >= maxRetries ){
                    pipelineLogger.error("Build failed More than maximum number of allowed times, cannot be retied")
                }
                throw e
            }
            else{
                tryAgain = utilities.getUserInputFailedBuild(config, "Deploy")
                numRetries+=1
            }
        }
    }
}