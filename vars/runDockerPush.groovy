
def push(config, serviceNames,role=null){
    def retry=true
    def retries=0
    def numAllowedRetries=2
    String pushRegistryUrl
    String pushRegistryCredsString
    (pushRegistryUrl, pushRegistryCredsString) = getRegistryProperties(config)
    //If this fails, retry in case of temporary issue with connection
    pipelineLogger.debug("pushregistryurl is ${pushRegistryUrl} & credstring is ${pushRegistryCredsString} ")
    while ( retry && retries < numAllowedRetries ) {
        try {
            if(role == null){
                docker.withRegistry("https://${pushRegistryUrl}", pushRegistryCredsString) {
                    pushImages(config, serviceNames, pushRegistryUrl)
                }                
            }else{
               ecrLogin(config,role)
                docker.withRegistry("https://${pushRegistryUrl}") {
                    pushImages(config, serviceNames, pushRegistryUrl)
                }
            }
            retry=false
        }
        catch (Exception e) {
            retries +=1
            if ( retries >= numAllowedRetries ) {
                throw e
            }
            sleep(10)
        }
    }
}


def pushImages(config, serviceNames, pushRegistryUrl){
    for (serviceName in serviceNames) {

        String imageName=utilities.generateDockerImageName(serviceName, config)
        String imageNameWithTag="${imageName}:${env.tag}"
                    
        pipelineLogger.debug("Pushing image '${imageNameWithTag}' into ${pushRegistryUrl}")
        docker.image(imageNameWithTag).push()
        pipelineLogger.debug("Pushed image '${pushRegistryUrl}/${imageNameWithTag}'")
        pipelineLogger.debug("Pushing tag 'latest' to point to '${env.tag}'")
        docker.image(imageNameWithTag).push("latest")
        pipelineLogger.debug("Image '${pushRegistryUrl}/${imageName}:latest' now refers to '${pushRegistryUrl}/${imageNameWithTag}'")
        // artifactoryServer = Artifactory.server "${env.artifactoryServer}" 
        //     try {
        //     	uploadArtifactoryMetadata(imageName, pushRegistryUrl)
        //     } catch(Exception ex) {
        //  		pipelineLogger.error("Failed while setting up the package properties at artifactory.")
		// 		pipelineLogger.info(ex.getMessage())
      	// 	}
    }
}


/*
    Stub method... fill this out later
    It should check whether an image with specific tag already exists in the registry or not - 
    we should only push image:tag combinations that do not yet exist (with exception of 'latest') 
*/
def validateOnPrem(Map config, List serviceNames) {
    /*

    */
    filteredImageNameToTagListMap=serviceNames
    return serviceNames
}




    /**
    * Performs two tasks for each image:
    *   1. Checks whether repository exists in ECR yet; creates if it does not exist
    *   2. Checks whether tag is already pushed - compiles list of serviceNames associated 
    *       with images that do NOT yet have the current build tag pushed into ECR 
    *
    *
    * @param config - map of properties read in from .env
    * @param serviceNames 

    */
 def validateEcr(Map config, List serviceNames, String role=null) {
    env.DockerImageInvocationRegistry="docker.io"
    //env.DockerImageInvocation="afi/devops/invocation-no-entrypoint:latest"
    env.DockerImageInvocation="devops/invocation-no-entrypoint:latest"
    env.DockerImageInvocationRegistryCreds=env.DOCKER_REG_CRED
    def fullInvocationImagePath = "${env.DockerImageInvocationRegistry}/${env.DockerImageInvocation}"
    def filteredImageNameToTagListMap=[:]
    List imagesToPush=[]
    pipelineLogger.debug("Attempting to validate properties around ECR docker registry.")
    docker.withRegistry("https://${env.DockerImageInvocationRegistry}", DockerImageInvocationRegistryCreds) {
        def awsRegion=config["awsRegion"]
        def awsUser=config["awsUser"]
        def awsAccountNumber=config["awsAccountNumber"]
        //docker.image(fullInvocationImagePath).inside("-e AWS_DEFAULT_REGION=${awsRegion}") {
        //    script {
                if(role != null){
                   def creds = utilAwsCmd.getRoleCredentials(awsAccountNumber,awsUser,role)
                   withEnv([
                        "AWS_ACCESS_KEY_ID=${creds.AccessKeyId}",
                        "AWS_SECRET_ACCESS_KEY=${creds.SecretAccessKey}",
                        "AWS_SESSION_TOKEN=${creds.SessionToken}"
                   ]) {
                      imagesToPush = generateImagesToPush(config,serviceNames)
                   } 
                }else{
                        pipelineLogger.debug(" checking for awsUser ${awsUser}")
                     withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: awsUser, accessKeyVariable: 'AWS_ACCESS_KEY_ID', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
                        imagesToPush = generateImagesToPush(config,serviceNames)
                     }
                }
               
         //   }
        //}
    }
    return imagesToPush
}

def generateImagesToPush(Map config, List serviceNames){
        List imagesToPush=[]
        for ( serviceName in serviceNames) {
                            
           String imageName=utilities.generateDockerImageName(serviceName, config)
           def awsUser= config["awsUser"] //added user to display in error
           def awsRegion = config["awsRegion"]
           //Check if repository has been created in ECR yet - if not, then create it now
          def checkRepositoryExists= sh script: "aws ecr describe-repositories --repository-names ${imageName} --region ${awsRegion} > /dev/null 2>&1 || aws ecr create-repository --repository-name ${imageName} --region ${awsRegion} > /dev/null 2>&1", returnStatus: true
          assert checkRepositoryExists == 0 :"ERROR: Error response while checking/creating repository in ECR.  Repository name ='${imageName}', using credentials id '${awsUser}'\nLikely issues are: \n-Invalid repository name\n-Invalid credentials/incorrect permissions associated with credentials.  Requires full permissions for ECR in order to run."
          def imageAlreadyExists = sh script: "aws ecr describe-images --repository-name ${imageName} --image-ids imageTag=${env.tag} --region ${awsRegion} > /dev/null 2>&1", returnStatus: true
          pipelineLogger.debug("imageAlreadyExists set to ${imageAlreadyExists}")
          if (imageAlreadyExists == 0) {
                pipelineLogger.warn("Image ${imageName} with tag ${env.tag} already exists in ECR.  This tag will not be pushed again")
          }
          else {
                pipelineLogger.debug("Image ${imageName}:${env.tag} was not found in ecr, so it can be pushed.")
                imagesToPush.add(serviceName)
          }
        }
        return imagesToPush
}


    /**
    * Generates map that holds image names and the asociated tags
    * Structured like:
    * ["brc/dpe-bloomreachexperience/develop/cms":["1.0.0", "latest"],
    *  "brc/dpe-bloomreachexperience/develop/site":["1.0.0", "latest"]
    * ]
    *
    * @param config - map of properties read in from .env
    * @param serviceNames - List of services defined in .env
    * @return imageNameToTagListMap - Map formatted as follows:
    * [<imagename1>:[<image1tag1>,<image1tag2>], <image2name>:[<image2tag1>, <image2tag2>]]
    * ['brc/dpe-bloomreachexperience/develop/cms':['akjllsdfjc9i3j2mx','latest']]
    *
    */
/*def generateImageNameToTagList(Map config, List serviceNames){
    def imageNameToTagListMap=[:]

    for (serviceName in serviceNames){
        def tagList = []
        imageName = utilities.generateDockerImageName(serviceName, config)

        if (env.tag != null && env.tag != "" && env.tag != "latest"){
            tagList.add(env.tag)
        }
        tagList.add("latest")
        imageNameToTagListMap.put(imageName, tagList)
    }
    return imageNameToTagListMap
}
*/


def getRegistryProperties(Map config){
    String pushRegistryUrl=""
    String credsString=""
    switch (config["dockerPushRegistryLocation"]){
        case "aws":
            def awsAccountNumber=config["awsAccountNumber"]
            def awsRegion=config["awsRegion"]
            def awsUser=config["awsUser"]

            pushRegistryUrl = getDockerPushUrlEcr(config)
            credsString = "ecr:${awsRegion}:${awsUser}"
            break
        case "rteja":
            pushRegistryUrl=getDockerPushUrlOnPrem(config)
            credsString=env.JENKINS_CREDENTIALS_RW
            break
        default:
            pipelineLogger.error("dockerPushRegistryLocation found in .env as '${dockerPushRegistryLocation}' which does not match any of the known registry types")
            break
    }
    return [pushRegistryUrl, credsString]    
}

def dockerPushEcr(Map config, List serviceNames,String role) {
    utilities.checkAwsVars(config)
    /*def awsAccountNumber=config["awsAccountNumber"]
    def awsRegion=config["awsRegion"]
    def awsUser=config["awsUser"]

    def pushRegistryUrl = getDockerPushUrlEcr(config)
    def credSetName = "ecr:${awsRegion}:${awsUser}"
    */
    String pushRegistryUrl
    String credSetName
    
    (pushRegistryUrl, credSetName) = getRegistryProperties(config)
    pipelineLogger(pushRegistryUrl)
    pipelineLogger(credSetName)
    //def imageNameToTagList = generateImageNameToTagList(config, serviceNames)
    
    //['brc/dpe-bloomreachexperience/develop/cms':['akjllsdfjc9i3j2mx','latest']]
    //def filteredImageNameToTagListMap = validateEcr(config, imageNameToTagList)
    List serviceNamesToPush=validateEcr(config, serviceNames,role)
    //push(filteredImageNameToTagListMap, pushRegistryUrl, credSetName)    
    push(config, serviceNamesToPush,role)
}


def getDockerPushUrl(Map config) {
    utilities.checkConfigVarExistsNotEmpty(config,"dockerPushRegistryLocation")
    String dockerPushUrl=""
    switch (config["dockerPushRegistryLocation"]){
        case "aws":
            dockerPushUrl= getDockerPushUrlEcr(config)
            break
        case "rteja":
            dockerPushUrl= getDockerPushUrlOnPrem(config)
            break
        default:
            pipelineLogger.error("dockerPushRegistryLocation found in .env as '${dockerPushRegistryLocation}' which does not match any of the known registry types")
            break
    }    
    return dockerPushUrl
}

def getDockerPushUrlEcr(Map config) {
    def awsAccountNumber=config["awsAccountNumber"]
    def awsRegion=config["awsRegion"]
    def pushRegistryUrl = "${awsAccountNumber}.dkr.ecr.${awsRegion}.amazonaws.com"
    return pushRegistryUrl
}

def getDockerPushUrlOnPrem(Map config) {
    // read from jenkins global variables.
    def pushRegistryUrl = (config['releaseBranch'] == env.gitBranchName) ? "${env.dockerPushReleaseRegistryUrl}":"${env.dockerPushDevRegistryUrl}"
    return pushRegistryUrl
}

def dockerPushOnPrem(Map config, List serviceNames){
    //the registry shoudl be based on env variable at build server. this is to restrict cross deployment. 
    //Stub method - this should be filled out to allow push to on-prem registry
    //Check variables in .env or generate based on branch name 
    
    //def pushRegistryUrl=getDockerPushUrlOnPrem(config)
    //def credSetName=env.JENKINS_CREDENTIALS_RW
    
    //def pushRegistryUrl, credSetName = getRegistryProperties(config)
    //def imageNameToTagList = generateImageNameToTagList(config, serviceNames)
    List imagesToPush= validateOnPrem(config, serviceNames)

    //push(filteredImageNameToTagListMap, pushRegistryUrl, credSetName)    
    push(config, imagesToPush)
}


  /**
    * Will take care of pushing docker images into the registry specified in properties
    *
    * Check the location of the docker push registry and switch to the correct function based on properties 
    *
    * @param config - Map of properties read in from properties file
    * @param serviceNames - List of service names parsed from config
    */
def call(Map config, List serviceNames, String role=null) {
    pipelineLogger.info("Entering Docker Push Stage")
    
    utilities.checkConfigVarExistsNotEmpty(config,"dockerPushRegistryLocation")

    switch (config["dockerPushRegistryLocation"]){
        case "aws":
            pipelineLogger.info("Found dockerPushRegistryLocation as 'aws'")
            dockerPushEcr(config, serviceNames,role)
            break
        case "rteja":
            pipelineLogger.info("Found dockerPushRegistryLocation as 'rteja'")

            dockerPushOnPrem(config, serviceNames)
            break
        default:
            pipelineLogger.error("dockerPushRegistryLocation found in .env as '${dockerPushRegistryLocation}' which does not match any of the known registry types")
            break
    }    

}

  /**
    * Wrapper around 'stageDockerPush' call method to run it inside a stage
    *
    * @param config - Map of properties read in from properties file
    * @param serviceNames - List of service names parsed from config
    */
def runStage(Map config, serviceNames=[]) {
    stage ('Docker Push') {
        script {
            def service_names = (serviceNames.isEmpty()) ? utilities.generateServiceNamesListFromConfigMap(config) : serviceNames
            runDockerPush(config,service_names)
        }
    }
}

/**
 * Using role credentials, do a ecr login to be able to docker commands against the registry
 */
def ecrLogin(config,awsRole){
    def awsAccountNumber=config["awsAccountNumber"]
    def awsRegion=config["awsRegion"]
    def awsUser=config["awsUser"]
    def creds = utilAwsCmd.getRoleCredentials(awsAccountNumber,awsUser,awsRole)
  try
  {
  
  
       withEnv([
            "AWS_ACCESS_KEY_ID=${creds.AccessKeyId}",
            "AWS_SECRET_ACCESS_KEY=${creds.SecretAccessKey}",
            "AWS_SESSION_TOKEN=${creds.SessionToken}"
          ])
          {
              sh(script:""" 
                        aws ecr get-login-password --region ${awsRegion} | docker login --username AWS --password-stdin ${awsAccountNumber}.dkr.ecr.${awsRegion}.amazonaws.com
                        """, returnStdout: true)
           }
    }
    catch (Exception ex) {
        pipelineLogger.fatal("AWS login failed")
      }
}
def uploadArtifactoryMetadata(imageName, pushRegistryUrl) {
  	// Set default security scan and environment variable values. 
    def (blackDuckValue, veracodeValue, sonarValue) = ["","",""]
    def deployedEnv=("${pushRegistryUrl}"=="dockerrelease.rteja.dev") ? "prod":"non-prod"
  	//pipelineLogger.debug("prodBranch is: '${prodBranch}'")
  	pipelineLogger.debug("deployedEnv is: '${deployedEnv}'")
	def targetrepo = ("${pushRegistryUrl}"=="dockerrelease.rteja.dev") ? "docker-local-release/":"docker-local-dev/"
    def reponame = scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().split("\\.")[0]
    pipelineLogger.debug("REPOSITORY NAME: '${reponame}'")
  	// Retrieve security scan booleans, if exist.
    if (env.blackduckEnabled) {
        blackDuckValue = env.blackduckEnabled
    } else blackDuckValue = "false"
    if (env.veracodeEnabled) {
        veracodeValue = env.veracodeEnabled
    } else veracodeValue = "false"
    if (env.sonarEnabled) {
        sonarValue = env.sonarEnabled
    } else sonarValue = "false"
  	
  	// This upload spec will be used to find the artifact in Artifactory we want to upload Properties to.
    def uploadSpec2 = 
        """{
            \"files\": [
            {
                \"pattern\": \"${targetrepo}${imageName}/${env.tag}/manifest.json\",
                \"target": \"${targetrepo}\"
            }
            ]
        }"""
  	pipelineLogger.debug("uploadSpec2 is: '${uploadSpec2}'")
  	pipelineLogger.info("BlackDuck scan performed: '${blackDuckValue}'")
  	pipelineLogger.info("Veracode scan performed: '${veracodeValue}'")
  	pipelineLogger.info("SonarQube scan performed: '${sonarValue}'")
  
  	// Add Properties in Artifactory.
    artifactoryServer.setProps spec: uploadSpec2, props: "environment=${deployedEnv};blackduck=${blackDuckValue};veracode=${veracodeValue};sonar=${sonarValue}; repositoryname=${reponame}"
}
